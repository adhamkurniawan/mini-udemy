class Task < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: :slugged

  belongs_to :course
  acts_as_list scope: :course
  has_many :user_tasks, dependent: :destroy

  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  validates :course_id, presence: true
  validates :title, presence: true
  validates :description, presence: true

  def next_task(tasks)
    tasks.where("position > ?", position).first
  end

  def previous_task(tasks)
    tasks.where("position < ?", position).last
  end
end
