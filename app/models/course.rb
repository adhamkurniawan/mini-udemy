class Course < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: :slugged

  has_many :tasks, -> { order(position: :asc) }, dependent: :destroy
  has_many :subscriptions, dependent: :destroy
  has_many :reviews, dependent: :destroy

  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  validates :title, presence: true, length: { maximum: 50 }
  validates :description, presence: true
  validates :price, presence: true, numericality: true

  def average_rating
    reviews.count == 0 ? "No Rating" : reviews.average(:star).round(2)
  end

  def paypal_link(user)
    subscription = Subscription.find_or_create_by(user: current_user, course_id: id)
    values = {
      :business => "adhamkurniawan29-facilitator@gmail.com",
      :cmd => "_xclick",
      :upload => 1,
      :amount => price,
      :notify_url => "https://d0636493.ngrok.io/payment_notification",
      :item_name => title,
      :item_number => subscription.id,
      :quantity => 1,
      :return => "https://d0636493.ngrok.io/my_courses"
    }.to_query
  end
end
