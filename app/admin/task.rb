ActiveAdmin.register Task do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  permit_params :course_id, :title, :description, :video_url, :image, :preview

  controller do
    def find_resource
      scoped_collection.friendly.find(params[:id])
    end
  end

  member_action :sort, method: :post do
    resource.set_list_position(params[:position])
  end

  member_action :move_to_top, method: :post do
    resource.move_to_top
    redirect_to admin_course_path(resource.course), notice: "#{resource.title} task moved to top."
  end

  index do
    id_column
    column :course_id
    column :title
    column :preview
    column :video_url
    actions
  end

  show do
    attributes_table do
      row :title
      row :description
      row :video_url
      row :image do
        task.image.present? ? image_tag(task.image.url, height: 300) : content_tag(:span, 'No image')
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :course_id, as: :select, collection: Hash[Course.all.map{ |c| [c.title, c.id] }], include_blank: false
      f.input :title
      f.input :description
      f.input :video_url
      f.input :image
      f.input :preview
      f.input :position
    end
    f.actions
  end
end
